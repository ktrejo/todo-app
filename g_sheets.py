from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

# Setup the Sheets API
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
store = file.Storage('credentials.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('sheets', 'v4', http=creds.authorize(Http()))

SPREADSHEET_ID = "ADD YOUR SPREADSHEET ID HERE"


def retrieve_task():
	"""
	Retrieve tasks currently on Google Sheets

	Returns
	-------
	list
		each entry of the list is another list containing the task details.

	"""

	RANGE_NAME = "Sheet1!A2:D"
	result = service.spreadsheets().values().get(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME)
	result = result.execute()

	values = result.get('values', [])

	return values



def upload_task(task_list):
	"""
	Upload task to Google Sheets. NOTE this will remove all current exisiting task on Google Sheet

	Parameters
	----------
	task_list : list
		A list of all task in the todo database. The first entry of the list is a list containing 
		the value ["START DATE", "DUE DATE", "TASKS", "STATUS"] which represents the header

	"""

	RANGE_NAME = "Sheet1!A1:D"

	# Clear current cells in sheets
	service.spreadsheets().values().clear(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME, body={}).execute()

	body = {
		"majorDimension":"ROWS",
		"values":task_list
	}

	# Load task to sheets
	service.spreadsheets().values().update(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME, 
											valueInputOption="USER_ENTERED", body=body).execute()

