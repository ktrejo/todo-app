This ToDo app is a task manager program which you can interact with it by your 
OS terminal. This app is capable of syncing your task with Google Sheets 


Requirements:

1) Python 3.+
 * Setup: https://www.python.org/downloads/

2) peewee
 * Setup: http://docs.peewee-orm.com/en/latest/peewee/installation.html

3) google-api-python-client
 * Also need to create/use Google Sheet API key
 * Setup: https://developers.google.com/sheets/api/quickstart/python