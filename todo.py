import os
import g_sheets as gs
from collections import OrderedDict
from datetime import datetime, timedelta
from peewee import *


db = SqliteDatabase("todo_list.db")


class ToDo(Model):
	"""
	This model represents To-Do task entry. end_date by default is a week after start_date.
	Completed is used to indicated that the task is finish and ready to erased
	"""

	task = CharField(max_length=256)
	start_date = DateTimeField(default=datetime.now())
	end_date = DateTimeField(default=datetime.now() + timedelta(days=7))
	completed = BooleanField(default=False)

	class Meta:
		database = db


class Setting(Model):
	"""This model represents the settings of the app"""

	sort_order= CharField(max_length=256, default="START DATE ASC")

	class Meta:
		database = db

class color():
	"""This class will add style and color to the terminal output"""

	PURPLE = "\033[95m"
	CYAN = "\033[96m"
	DARKCYAN = "\033[36m"
	BLUE = "\033[94m"
	GREEN = "\033[92m"
	ELLOW = "\033[93m"
	RED = "\033[91m"
	BOLD = "\033[1m"
	UNDERLINE = "\033[4m"
	END = "\033[0m"


def initialize():

	db.connect()
	db.create_tables([ToDo, Setting])

	# creates an entry containing the default settings.
	if len(Setting.select()) == 0:
		Setting.create()



def clear_screen():
	"""Clears the terminal"""

	os.system('cls' if os.name == 'nt' else 'clear')
	


def terminal_width():
	"""Get width dimension of terminal"""

	columns, rows = os.get_terminal_size()

	return int(columns)



def add_task(index, tasks):
	"""Add new task

	Parameters
	----------
	index : int
		selected position in tasks
	tasks : list
		list of all task
	"""

	clear_screen()

	task = input("Enter task:")
	end_date = input("Enter deadline in number of days (by default 7 days):")

	new_entry = None

	if end_date.isdigit():
		end_date = int(end_date)
		new_entry = ToDo.create(task=task, end_date=datetime.now() + timedelta(days=end_date))

	else:
		end_date = 7
		new_entry = ToDo.create(task=task)



def toggle_done(task):
	"""
	Toggle done status

	Parameters
	----------
	task : ToDo
		A ToDo entry
	"""

	task.completed = not task.completed
	task.save()


def edit_task_name(task):
	"""
	Edit task description

	Parameters
	----------
	task : ToDo
		A ToDo entry
	"""

	clear_screen()
	view_tasks(0, [task])

	info = input("New task description:")[0:255]
	task.task = info

	task.save()


def change_due_date(task):
	"""
	Change due date

	Parameters
	----------
	task : ToDo
		A ToDo entry
	"""

	clear_screen()
	view_tasks(0, [task])

	day = input("Change due date (in number of days) starting from {:%B %d, %Y}:".format(task.start_date)).strip()
	
	if day.isdigit():
		day = int(day)
		task.end_date = task.start_date + timedelta(days=day)
		task.save()



def delete_task(task):
	"""
	Delete current task

	Parameters
	----------
	task : ToDo
		A ToDo entry
	"""

	clear_screen()
	view_tasks(0, [task])

	action = input("Are you sure? [y/n]").strip().lower()
	if action == "y" or action == "yes":
		task.delete_instance()



def modify_task(index, tasks):
	"""
	Modify current task

	Parameters
	----------
	index : int
		selected position in tasks
	tasks : list
		list of all task
	"""
	
	clear_screen()
	task = tasks[index]
	view_tasks(0, [task])

	for key, value in modify_task_menu.items():
			print("{}) {}".format(key, value.__doc__))

			# Only output toggle done only if task is completed
			if task.completed and key == "t":
				break

	print("g) Go back\n")

	action = input("Select Action:")
	if action in modify_task_menu:
		modify_task_menu[action](task)

	else:
		return



def remove_completed(index, tasks):
	"""
	Remove all completed tasks

	Parameters
	----------
	index : int
		selected position in tasks
	tasks : list
		list of all task
	"""

	clear_screen()

	action = input("Are you sure? [y/n]").strip().lower()
	if action == "y" or action == "yes":
		ToDo.delete().where(ToDo.completed == True).execute()



def sort_tasks():
	"""Sort tasks"""

	clear_screen()

	print("1) Sort by starting date")
	print("2) Sort by starting date reverse")
	print("3) Sort by ending date")
	print("4) Sort by ending date reverse")

	num = input("\nSelect Action:").strip().lower()

	# Ignore and return from bad input
	if not num.isdigit() or int(num) > 4:
		return

	num = int(num)
	order = Setting.select()[0]

	if num == 1:
		order.sort_order = "START DATE ASC"

	elif num == 2:
		order.sort_order = "START DATE DESC"

	elif num == 3:
		order.sort_order = "END DATE ASC"

	else:
		order.sort_order = "END DATE DESC"

	order.save()



def settings_menu():
	"""Settings"""

	clear_screen()

	print("c) Change order of task")
	print("g) Go back")

	action = input("\nSelect Action:").strip().lower()

	if action == "c":
		sort_tasks()

	else:
		return



def download_tasks():
	"""Download tasks from Google Sheets"""

	clear_screen()

	ToDo.delete().execute()

	print("Downloading tasks...")

	lst = gs.retrieve_task()

	for START, DUE, TASK, STATUS in lst:
		# Convert the status colume value into a boolean value
		completed = STATUS == "DONE"
		# Covert both date to datetime object
		start_date = datetime.strptime(START, "%m/%d/%y")
		end_date = datetime.strptime(DUE, "%m/%d/%y")

		ToDo.create(task=TASK, start_date=start_date, end_date=end_date, completed=completed)

	print("Tasks downloaded")




def upload_tasks():
	"""Upload tasks to Google Sheets"""

	clear_screen()

	todo_list = ToDo.select().order_by(ToDo.start_date.asc())
	task_list = [["START DATE", "DUE DATE", "TASKS", "STATUS"]]

	for task in todo_list:
		# format start and end date to mm-dd-yy
		start = "{:%m/%d/%y}".format(task.start_date)
		end = "{:%m/%d/%y}".format(task.end_date)
		info = task.task
		status = "DONE" if task.completed else "Not Finish"
		# print("{} {} {} {}".format(start, end, info, status))

		task_list.append([start, end, info, status])


	print("Uploading tasks...")

	gs.upload_task(task_list)

	print("Tasks uploaded")
	input("hit enter")




def sync():
	"""Upload/Retrieve your tasks"""

	clear_screen()

	for key, value in sync_menu.items():
		print("{}) {}".format(key, value.__doc__))

	print("g) Go back")

	action = input("Select Action:").strip().lower()

	if action in sync_menu:
		sync_menu[action]()




def get_tasks():
	"""Get tasks base on the sorting criteria under settings"""

	tasks = None
	sort_order = Setting.select()[0].sort_order
	print("get_task sort_order: {}".format(sort_order))

	if sort_order == "START DATE ASC":
		tasks = ToDo.select().order_by(ToDo.start_date.asc())

	elif sort_order == "START DATE DESC":
		tasks = ToDo.select().order_by(ToDo.start_date.desc())

	elif sort_order == "END DATE ASC":
		tasks = ToDo.select().order_by(ToDo.end_date.asc())

	elif sort_order == "END DATE DESC":
		tasks = ToDo.select().order_by(ToDo.end_date.desc())

	else:
		print("ERROR unknown sort criteria")
		tasks = []

	return tasks




def view_tasks(index, tasks):
	"""
	View tasks in menu

	Parameters
	----------
	index : int
		selected position in tasks
	tasks : list
		list of all task
	"""

	date_time = None
	current_date = datetime.now()
	t_width = terminal_width()

	# Add style and color to the status of each task
	late_message = color.RED + color.BOLD + "*** LATE *** " + color.END
	done_message = color.GREEN + "(DONE)" + color.END 

	for pos, task in enumerate(tasks):
		# format to be month date, year
		starting_date = "{:%B %d, %Y}".format(task.start_date)
		ending_date = "{:%B %d, %Y}".format(task.end_date)

		if date_time != starting_date:
			# add bold and underline to each date
			print("\t" + color.BOLD + color.UNDERLINE + starting_date + color.END + "\n")
			# print("=" * t_width)
			date_time = starting_date

		# Indicates the current selected task
		prefix = ">>>>" if pos == index else "•   "
		late = late_message if current_date > task.end_date and not task.completed else ""

		message = "{}{}{} \n    DUE DATE: {} ".format(prefix, late, task.task, ending_date)

		if task.completed:
			message += done_message

		message += "\n"
		print(message)



def menu():
	"""Main menu of the app"""

	action = None
	index = 0
	t_width = terminal_width()

	# List of all current task entries currently in the db
	tasks = get_tasks()

	while action != 'q':
		clear_screen()
		if len(tasks) != 0:
			view_tasks(index, tasks)
			# Seperate tasks menu and action menu
			print("=" * t_width)

		# display the action menu
		for key, value in main_menu.items():
			print("{}) {}".format(key, value.__doc__))

		for key, value in main_menu2.items():
			print("{}) {}".format(key, value.__doc__))

		print("\np) Prev\nn) Next")
		print("\nq) Quit\n")

		action = input("Select Action:").strip().lower()

		if action in main_menu:
			main_menu[action](index, tasks)
			tasks = get_tasks()
			index = index % len(tasks)

		elif action in main_menu2:
			main_menu2[action]()
			tasks = get_tasks()

		elif action == "n":
			index = (index + 1) % len(tasks)

		elif action == "p":
			index = (index - 1) % len(tasks)



main_menu = OrderedDict([
    ("a", add_task),
    ("m", modify_task),
    ("r", remove_completed)
])

main_menu2 = OrderedDict([
	("u", sync),
	("s", settings_menu)
])


modify_task_menu = OrderedDict([
	("t", toggle_done),
	("e", edit_task_name),
	("c", change_due_date),
	("d", delete_task)
])


sync_menu = OrderedDict([
	("u", upload_tasks),
	("d", download_tasks)
])



if __name__ == '__main__':

    initialize()
    clear_screen()
    menu()
    db.close()